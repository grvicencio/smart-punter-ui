<html>
<head>

    @include('layouts.header-script')
    <style>
        @media (min-width: 992px) {
            header.masthead {
                height: 300px;
                padding-top: 0;
                padding-bottom: 0;
                margin-top: -119px;
                z-index: 0;
                background: url("{{asset("assets/banner/banner2.png")}}") no-repeat 0px 0px;
                background-size: 100%;
            }
        }

    </style>

</head>
<body>
<div class="wrapper">
    @include('layouts.header')
    <header class="masthead">
        <div class="container-fluid">
            <div class="row">
                <div class="offset-lg-1 offset-md-1 offset-sm-1 col-lg-8 col-md-8 col-sm-8 my-auto" style="    margin-top: 8% !important;">
                    <div class="header-content mx-auto">

                        <img class="img-fluid" src="{{asset("assets/text-img/contact.png")}}">
                    </div>
                </div>
                <div class="col-sm-3 col-lg-3 col-md-3 my-auto" style="    margin-top: 8% !important;">
                    <img src="assets/join-us-logo.png" class="img-fluid" alt="">
                </div>
            </div>
        </div>
    </header>

    <div class="container-fluid" id="main-contact-us-container">
        <div class="row">
            <div class="col-xs-12 offset-lg-1 col-lg-11 my-auto">
                <div class="main-contact-us-title">
                    Our operating hours are 9am to 5am, please feel free to contact our friendly customer<br>
                    service representatives during these hours for any assistance

                    <br>
                    <br>
                    <br>

                    <p class="contactus-text"><img src="{{asset("assets/icons/telephone-icon.png")}}" alt="" class="contact-icon" align="middle">+65 9876 1234</p>
                    <p class="contactus-text"><img src="{{asset("assets/icons/email-icon.png")}}" alt="" class="contact-icon" align="middle">sp88bet@gmail.com</p>
                    <p class="contactus-text"><img src="{{asset("assets/icons/line-icon.png")}}" alt="" class="contact-icon" align="middle">+65 9876 1234</p>
                    <p class="contactus-text"><img src="{{asset("assets/icons/wechat-icon.png")}}" alt="" class="contact-icon" align="middle">+65 9876 1234</p>
                </div>

            </div>

        </div>

    </div>
@include('layouts.footer')
@include('layouts.footer-script')
</body>

</html>
