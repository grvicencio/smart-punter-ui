<html>
<head>

    @include('layouts.header-script')
    <style>
        @media (min-width: 992px) {
            header.masthead {
                height: 300px;
                padding-top: 0;
                padding-bottom: 0;
                margin-top: -119px;
                z-index: 0;
                background: url("{{asset("assets/banner/banner2.png")}}") no-repeat 0px 0px;
                background-size: 100%;
            }
        }

        header.masthead .header-content {
            margin-bottom: 0;
            text-align: left;
            display: flex;
            /* width: 50%; */
            height: 100%;
            margin: auto;
            border-radius: 10px;
            /* border: 3px dashed #1c87c9; */
        }
    </style>


</head>
<body>
<div class="wrapper">
    @include('layouts.header')
    <header class="masthead">
        <div class="container-fluid">
                    <div class="header-content">
                        <p class="dashboard-points"> 132161343<br><span class="dashboard-points-available">AVAILABLE</span></p>
                    </div>

        </div>
    </header>

    <div class="container-fluid" id="main-dashboard-container">
        <div class=" offset-lg-1 col-lg-11 my-auto">
            <div class="header-content mx-auto">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link  dashboard-tab-link active" data-toggle="tab" href="#home">SHOP</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link dashboard-tab-link " data-toggle="tab" href="#menu1">LIVE STREAM</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link dashboard-tab-link " data-toggle="tab" href="#menu2">GAMES</a>
                    </li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active" id="home">
                    <div class="row row-product">
                        <div class=" col-xs-12 col-sm-3">
                            <div class="card card-product">
                                <img class="card-img-top" src="{{asset("assets/products/iphone.png")}}" alt="Card image cap">
                                <div class="card-body-product">
                                    <p class="card-title card-title-product">IPHONE XMAX <span class="card-product-redeemed">18 REDEEMED </span></p>
                                </div>
                            </div>
                        </div>
                        <div class=" col-xs-12 col-sm-3">
                            <div class="card card-product">
                                <img class="card-img-top" src="{{asset("assets/products/samsung.png")}}" alt="Card image cap">
                                <div class="card-body-product">
                                    <p class="card-title card-title-product">SAMSUNG <span class="card-product-redeemed">12 REDEEMED </span></p>
                                </div>
                            </div>
                        </div>

                        <div class=" col-xs-12 col-sm-3">
                            <div class="card card-product">
                                <img class="card-img-top" src="{{asset("assets/products/escooter.png")}}" alt="Card image cap">
                                <div class="card-body-product">
                                    <p class="card-title card-title-product">ESCOOTER XMAX <span class="card-product-redeemed">5 REDEEMED </span></p>
                                </div>
                            </div>
                        </div>

                    </div>
                        <div class="row row-product">
                            <div class=" col-xs-12 col-sm-3">
                                <div class="card card-product">
                                    <img class="card-img-top" src="{{asset("assets/products/iphone.png")}}" alt="Card image cap">
                                    <div class="card-body-product">
                                        <p class="card-title card-title-product">IPHONE XMAX <span class="card-product-redeemed">18 REDEEMED </span></p>
                                    </div>
                                </div>
                            </div>
                            <div class=" col-xs-12 col-sm-3">
                                <div class="card card-product">
                                    <img class="card-img-top" src="{{asset("assets/products/samsung.png")}}" alt="Card image cap">
                                    <div class="card-body-product">
                                        <p class="card-title card-title-product">SAMSUNG <span class="card-product-redeemed">12 REDEEMED </span></p>
                                    </div>
                                </div>
                            </div>

                            <div class=" col-xs-12 col-sm-3">
                                <div class="card card-product">
                                    <img class="card-img-top" src="{{asset("assets/products/escooter.png")}}" alt="Card image cap">
                                    <div class="card-body-product">
                                        <p class="card-title card-title-product">ESCOOTER XMAX <span class="card-product-redeemed">5 REDEEMED </span></p>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row row-product">
                            <div class=" col-xs-12 col-sm-3">
                                <div class="card card-product">
                                    <img class="card-img-top" src="{{asset("assets/products/iphone.png")}}" alt="Card image cap">
                                    <div class="card-body-product">
                                        <p class="card-title card-title-product">IPHONE XMAX <span class="card-product-redeemed">18 REDEEMED </span></p>
                                    </div>
                                </div>
                            </div>
                            <div class=" col-xs-12 col-sm-3">
                                <div class="card card-product">
                                    <img class="card-img-top" src="{{asset("assets/products/samsung.png")}}" alt="Card image cap">
                                    <div class="card-body-product">
                                        <p class="card-title card-title-product">SAMSUNG <span class="card-product-redeemed">12 REDEEMED </span></p>
                                    </div>
                                </div>
                            </div>

                            <div class=" col-xs-12 col-sm-3">
                                <div class="card card-product">
                                    <img class="card-img-top" src="{{asset("assets/products/escooter.png")}}" alt="Card image cap">
                                    <div class="card-body-product">
                                        <p class="card-title card-title-product">ESCOOTER XMAX <span class="card-product-redeemed">5 REDEEMED </span></p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    <div class="tab-pane container fade" id="menu1">...</div>
                    <div class="tab-pane container fade" id="menu2">...</div>
                </div>

            </div>
        </div>


    </div>
@include('layouts.footer')
@include('layouts.footer-script')
</body>

</html>
