<html>
<head>

    @include('layouts.header-script')
    <style>
        @media (min-width: 992px) {
            header.masthead {
                height: 300px;
                padding-top: 0;
                padding-bottom: 0;
                margin-top: -119px;
                z-index: 0;
                background: url("{{asset("assets/banner/banner2.png")}}") no-repeat 0px 0px;
                background-size: 100%;
            }
        }

    </style>

</head>
<body>
<div class="wrapper">
    @include('layouts.header')
    <header class="masthead">
        <div class="container-fluid">
            <div class="row">
                <div class="offset-lg-1 offset-md-1 offset-sm-1 col-lg-8 col-md-8 col-sm-8 my-auto" style="    margin-top: 8% !important;">
                    <div class="header-content mx-auto">

                        <img class="img-fluid" src="{{asset("assets/text-img/aboutus.png")}}">
                    </div>
                </div>
                <div class="col-sm-3 col-lg-3 col-md-3 my-auto" style="    margin-top: 8% !important;">
                    <img src="assets/join-us-logo.png" class="img-fluid" alt="">
                </div>
            </div>
        </div>
    </header>

    <div class="container-fluid" id="main-about-container">

        <div class=" offset-lg-1 col-lg-11 my-auto">
            <div class="header-content mx-auto">

                <p class="about-description">
                <span class="about-title">Duis eget tellus sed mauris condimentum </span>
                    <br>
                    <br>
                    Maecenas volutpat suscipit felis nec laoreet. Interdum et malesuada fames ac ante ipsum primis in faucibus. Quisque accumsan eros a rutrum consequat.
                    <br>
                    Suspendisse faucibus arcu sit amet ligula bibendum, non suscipit tellus dapibus. Suspendisse suscipit ipsum purus, quis tincidunt sapien ornare at. Proin
                    <br>
                    metus sapien, gravida non purus eget, suscipit interdum mi. Nulla eget mollis nunc. Praesent fringilla orci tellus, nec elementum purus iaculis nec. Aliquam
                    <br>
                    eget diam sed nisl tincidunt varius. Donec blandit bibendum sapien, non varius dolor pellentesque non. Sed aliquam eget dui in porttitor. Donec rhoncus
                    <br>
                    eros et vulputate faucibus. Interdum et malesuada fames ac ante ipsum primis in faucibus.
                    <br>
                    <br>
                    <span class="about-title">Duis eget tellus sed mauris condimentum </span>
                    <br>
                    <br>
                    Maecenas volutpat suscipit felis nec laoreet. Interdum et malesuada fames ac ante ipsum primis in faucibus. Quisque accumsan eros a rutrum consequat.
                    <br>
                    Suspendisse faucibus arcu sit amet ligula bibendum, non suscipit tellus dapibus. Suspendisse suscipit ipsum purus, quis tincidunt sapien ornare at. Proin
                    <br>
                    metus sapien, gravida non purus eget, suscipit interdum mi. Nulla eget mollis nunc. Praesent fringilla orci tellus, nec elementum purus iaculis nec. Aliquam
                    <br>
                    <br>
                    <span class="about-title">Duis eget tellus sed mauris condimentum </span>
                    <br>
                    Vivamus nec interdum arcu. Vestibulum sit amet aliquam nisi, sit amet malesuada neque. Fusce nec molestie mi, a placerat sapien. Etiam lobortis, diam a
                    <br>
                    laoreet fermentum, nibh elit dignissim sapien, at tincidunt lacus turpis eu nibh. Nullam nec nisl id nisi dictum commodo sit amet pellentesque nisi. Morbi
                    <br>
                    nibh eros, mattis sed bibendum ut, tempus vel sem. Etiam rutrum, turpis id rutrum convallis, massa sem consequat elit, a sagittis enim mi et nunc. Sus
                    <br>
                    pendisse facilisis orci vel tellus tempor, in finibus nulla hendrerit. Praesent commodo eget sem a lacinia. Vivamus vitae orci ante. Donec interdum finibus
                    <br>
                    ligula non aliquet.
                </p>
            </div>
        </div>


    </div>
@include('layouts.footer')
@include('layouts.footer-script')
</body>

</html>
