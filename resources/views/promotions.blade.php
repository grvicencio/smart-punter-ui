<html>
<head>

    @include('layouts.header-script')
    <style>
        @media (min-width: 992px) {
            header.masthead {
                height: 300px;
                padding-top: 0;
                padding-bottom: 0;
                margin-top: -119px;
                z-index: 0;
                background: url("{{asset("assets/banner/banner2.png")}}") no-repeat 0px 0px;
                background-size: 100%;
            }
        }

    </style>

</head>
<body>
<div class="wrapper">
    @include('layouts.header')
    <header class="masthead">
        <div class="container-fluid">
            <div class="row">
                <div class="offset-lg-1 offset-md-1 offset-sm-1 col-lg-8 col-md-8 col-sm-8 my-auto" style="    margin-top: 8% !important;">
                    <div class="header-content mx-auto">

                        <img class="img-fluid" src="{{asset("assets/text-img/promotions.png")}}">
                    </div>
                </div>
                <div class="col-sm-3 col-lg-3 col-md-3 my-auto" style="    margin-top: 8% !important;">
                    <img src="assets/join-us-logo.png" class="img-fluid" alt="">
                </div>
            </div>
        </div>
    </header>

    <div class="container-fluid" id="main-promotions-container">
        <div class="row promotion-row">
            <div class="col-xs-12 offset-lg-1 offset-lg-1 col-lg-3 col-md-3 col-sm-4 my-auto promotion-thumbnail">
                <img class="img-promotion img-fluid" src="{{asset("assets/promotions/promotion1.jpg")}}">
            </div>
            <div class="col-xs-12 col-sm-8 col-lg-7 col-md-7 ">
                <div class="promotion-title">OPEN ACCOUNT BONUS</div>
                <div class="promotion-description">MEMBERS CAN ONLY WITHDRAW THEIR DEPOSIT BONUS AFTER X 4 TURNOVER OF
                    THEIR DEPOSITED<br>AMOUNT.EXAMPLE: FIRST DEPOSIT IS $100, YOU HAVE TO TURNOVER $100×4= $400..
                </div>
            </div>
        </div>
        <div class="row promotion-row">
            <div class="col-xs-12 offset-lg-1 offset-lg-1 col-lg-3 col-md-3 col-sm-4 my-auto promotion-thumbnail">
                <img class="img-promotion img-fluid" src="{{asset("assets/promotions/promotion3.jpg")}}">
            </div>
            <div class="col-xs-12 col-sm-8 col-lg-7 col-md-7 ">
                <div class="promotion-title">WEEKLY TOP 3 PLAYERS TURNOVER PROMOTION</div>
                <div class="promotion-description">EVERY MONDAYS THERE WILL BE 3 PRIZES TO GIVEN AWAY FOR OUT TOP 3
                    HIGHEST TURNOVER MEMBERS.
                    WINNERS WILL BE NOTIFY BY SMS AND CREDITS WILL BE CREDIT INTO ACCOUNT ON EVERY MONDAYS BY 6PM.
                    <div>
                    </div>


                </div>
            </div>
        </div>
        <div class="row promotion-row">
            <div class="col-xs-12 offset-lg-1 offset-lg-1 col-lg-3 col-md-3 col-sm-4 my-auto promotion-thumbnail">
                <img class="img-promotion img-fluid" src="{{asset("assets/promotions/promotion2.jpg")}}">
            </div>
            <div class="col-xs-12 col-sm-8 col-lg-7 col-md-7 ">
                <div class="promotion-title">WEEKLY LOSS 5% CASH REBATE PROMOTION</div>
                <div class="promotion-description">EVERY MONDAYS WE WILL CALCULATE ON THE TOTAL LOSSES FOR THE WEEK AND AUTO REBATE A 5% CREDITS INTO YOUR ACCOUNT BY 6PM.ALL PROMOTIONS ABOVE ARE ONLY FOR SPORTSBOOK ONLY STARTING FROM 2 DECEMBER 2013 TILL FURTHER NOTICE.
                    <div>
                    </div>


                </div>
            </div>
        </div>
    </div>
@include('layouts.footer')
@include('layouts.footer-script')
</body>

</html>
