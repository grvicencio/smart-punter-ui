<html>
<head>

    @include('layouts.header-script')
    <style>
        @media (min-width: 992px) {
            header.masthead {
                height: 300px;
                padding-top: 0;
                padding-bottom: 0;
                margin-top: -119px;
                z-index: 0;
                background: url("{{asset("assets/banner/banner2.png")}}") no-repeat 0px 0px;
                background-size: 100%;
            }
        }

    </style>

</head>
<body>
<div class="wrapper">
    @include('layouts.header')
    <header class="masthead">
        <div class="container-fluid">
            <div class="row">
                <div class="offset-lg-1 offset-md-1 offset-sm-1 col-lg-8 col-md-8 col-sm-8 my-auto" style="    margin-top: 8% !important;">
                    <div class="header-content mx-auto">

                        <img class="img-fluid" src="{{asset("assets/text-img/terms.png")}}">
                    </div>
                </div>
                <div class="col-sm-3 col-lg-3 col-md-3 my-auto" style="    margin-top: 8% !important;">
                    <img src="assets/join-us-logo.png" class="img-fluid" alt="">
                </div>
            </div>
        </div>
    </header>

    <div class="container-fluid" id="main-terms-container">

        <div class=" offset-lg-1 col-lg-11 my-auto">
            <div class="header-content mx-auto">

                <p class="terms-description">
                <span class="terms-title">Terms &amp; Conditions </span><br><br>

                You must be 18 and above to open an account and deposit with SmartPunter. You are responsible for abiding by the minimum age <br>gambling laws in your relevant jurisdiction.
                <br>
                <br>
                All members are allowed only one account per product, and one phone number and bank account to be linked to their name. Multiple<br>signups by the same member are not allowed.
                Withdrawals may only be made to the registered bank account associated with the member.
                <br>
                <br> All members are responsible for keeping his or her own User ID’s and passwords confidential.
                <br>
                <br> All details of members are kept highly confidential, and are not released or sold for any purpose whatsoever.
                <br>
                <br> All members are responsible for keeping SmartPunter up to date if there are any changes in contact information.Withdrawals can only be<br>processed through verified numbers and email. E.g. if you are changing your phone number, please notify our customer service<br>representative with your current number before changing it. For security purposes, we reserve the right to reject withdrawals should there<br>be any discrepancies in the information provided during our verification process.
                <br>
                <br>

                <span class="terms-title">SmartPunter and its affiliate gaming providers reserve the right to reject and/or void:</span> <br>

                <br> - Any member accounts being used for syndicated bets
                <br>
                <br> - Bets suspected of being syndicated bets
                <br>
                <br>- Abuse of rebates/bonuses and SPclub Points,
                <br>
                <br> - Bets made or facilitated by bots and programs, automated or otherwise.
                <br>
                <br>SmartPunter reserves the right to withhold a withdrawal if the account is under investigation for any fraudulent activities.
                <br> SmartPunter adheres to the rules and regulations set forth by our affiliate gaming provider partners. In the event of a disagreement or<br>dispute, we will forward our member’s complaints to the respective support teams and provide our full support to address the situation<br>and keep our member informed of the outcome of the decision. Upon due diligence, SmartPunter reserves the right to follow our partners’<br>final decision.


                </p>
            </div>
        </div>


    </div>
@include('layouts.footer')
@include('layouts.footer-script')
</body>

</html>
