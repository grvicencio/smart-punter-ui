<?php
/**
 * Created by PhpStorm.
 * User: NPT 8
 * Date: 10/18/2018
 * Time: 2:36 PM
 */
?>


<div class="footer">
    <div class="container-fluid footer-content" style="padding-left:0px !important; padding-right:0px !important;">

        <div>
            <img id="footer1" src="{{ asset("assets/footer.png")}}">
        </div>

        <div id="blue_footer_div">
            <div class="container">
                <div class="row ">
                    <div class="col-sm-3">
                        <div class="footer-title">
                            QUICK LINKS
                        </div>
                        <div class="footer-links">
                            <ul>
                                <li><a  class="footer-links" href="/">Home</a></li>
                                <li><a  class="footer-links" href="/contactus">About Us</a></li>
                                <li><a  class="footer-links" href="/promotions">Promotions</a></li>
                                <li><a  class="footer-links" href="/">SP Club</a></li>
                                <li><a  class="footer-links"  href="/">Club</a></li>
                            </ul>
                        </div>

                    </div>
                    <div class="col-sm-4">
                        <div class="footer-title">
                            Website 100% Secure
                        </div>
                        <div class="footer-links">
                            <ul>
                                <li><a class="footer-links" href="">Casino with DGOJ license</a> </li>
                                <li><a class="footer-links" href="">Software Certificated</a> </li>
                                <li><a class="footer-links" href="">Secure Transactions</a> </li>
                                <li><a class="footer-links" href="">Live Scores</a> </li>
                                <li><a class="footer-links" href="">Bonus Upon Registration</a> </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="footer-title">
                            Payments Accepted
                        </div>

                        <div class="links-payment">
                            <a href="#"><img class="img-footer-accepted-payment"
                                             src="{{asset("assets/payment-methods/Visa.png")}}"> </a>
                            <a href="#"> <img class="img-footer-accepted-payment"
                                              src="{{asset("assets/payment-methods/Mastercard.png")}}"> </a>
                            <a href="#"><img class="img-footer-accepted-payment"
                                             src="{{asset("assets/payment-methods/Amex.png")}}"> </a>
                            <a href="#"><img class="img-footer-accepted-payment"
                                             src="{{asset("assets/payment-methods/Paypal.png")}}"> </a>
                            <a href="#"><img class="img-footer-accepted-payment"
                                             src="{{asset("assets/payment-methods/Maestro.png")}}"> </a>
                        </div>


                        <div class="footer-title">
                            Subscribe for Offers
                        </div>

                        <div class="footer-subscribe">
                            <div class="input-group">

                                <input id="email_subscribe" type="text" class="form-control" name="email" placeholder="Email">
                                <span class="input-group-addon"><img src="{{asset("assets/icons/message.png")}}"></span>
                            </div>
                        </div>


                    </div>

                </div>


            </div>


        </div>

    </div>
    <div class=" text-center footer-c">
        <hr class="hr-footer">

        © {{  date("Y")  }} SmartPunters All Rights Reserved

    </div>


</div>
