<?php
/**
 * Created by PhpStorm.
 * User: NPT 8
 * Date: 10/18/2018
 * Time: 2:35 PM
 */?>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=yes">

<meta name="description" content="">
<meta name="author" content="">

<title>New Age - Start Bootstrap Theme</title>

<link rel="stylesheet" href="vendor/simple-line-icons/css/simple-line-icons.css">
<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">



<!-- Custom styles for this template -->
<link href="css/main.css" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="{{asset("css/bootstrap.css")}}" >