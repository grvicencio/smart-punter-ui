<?php
/**
 * Created by PhpStorm.
 * User: NPT 8
 * Date: 10/18/2018
 * Time: 2:35 PM
 */

?>
{{--<nav class="navbar navbar-expand-lg" id="mainNav">--}}
{{--<div class="container">--}}
{{--<hr style="width: 100% !important; max-width: unset;">--}}
{{--</div>--}}
{{--</nav>--}}

<nav class="navbar navbar-expand-lg" id="mainNav" style="border-bottom: #e9e9e9 solid 1px;">
    <div class="container-fluid">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#mainNav"
                aria-controls="mainNav" aria-expanded="false" aria-label="Toggle navigation">
            Menu
            <i class="fas fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbar-Responsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link header1" href="#download"><img class="header-icon"
                                                                      src="{{asset("assets/icons/icon-safe.png")}}">
                        Safe & Secure</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link header1" href="#features"><img class="header-icon"
                                                                      src="{{asset("assets/icons/icon-support.png")}}">
                        Support 24/7</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link header1" href="#contact"><img class="header-icon"
                                                                     src="{{asset("assets/icons/icon-login.png")}}">
                        Login / Register</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<nav class="navbar navbar-expand-lg" id="mainNav2">
    <div class="container-fluid offset-lg-1 col-lg-10 my-auto">
                <a class="navbar-brand " href="#page-top"><img src="{{asset("assets/logo-nav.png")}}"></a>
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                        data-target="#navbarResponsive2" aria-controls="navbarResponsive2" aria-expanded="false"
                        aria-label="Toggle navigation">
                    Menu
                    <i class="fas fa-bars"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbar-Responsive2">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link header2" href="/aboutus">ABOUT US</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link header2" href="/promotions">PROMOTIONS</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link header2" href="#contact">SP CLUB</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link header2" href="/contactus">CONTACT</a>
                        </li>

                    </ul>
                </div>




    </div>

</nav>






