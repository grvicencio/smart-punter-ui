<html>
<head>

    @include('layouts.header-script')
<style>
    header.masthead {
        height: 66%;
    }
    </style>
</head>
<body>
<div class="wrapper">
    @include('layouts.header')
    <header class="masthead">
        <div class="container-fluid">
            <div class="row offset-lg-9 offset-xs-9 col-lg-3 offset-sm-9 col-lg-3 offset-md-9 col-lg-3 col-md-3 col-sm-3 col-xs-3 join-us-button" >
                <img src="assets/join-us-logo.png" class="img-fluid join-us-logo" alt="">
            </div>
            <div class="row h-100">
                <div class=" offset-lg-4 col-lg-5 offset-md-6 col-md-5 offset-sm-4 col-sm-5 ">
                    <div class="header-content">
                        <h1 class="mb-5"><img class="img-fluid" src="assets/text-img/welcome.png"></h1>
                        <div class="main-description">
                            Here at SP, with our promotions, rebates and<br>
                            SP club membership, we promise you the<br>
                            most value for your dollar.<br>
                        </div>
                        <br>
                        <a href="#download" class="btn btn-outline btn-xl js-scroll-trigger">JOIN US TODAY!</a>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="container-fluid">
        <div id="blue_div">
            <div class="container text-center main-footer-container">
                <table width="100%">
                    <th>
                        <tr>
                            <td class="text-footer-like-container">

                                <span class="main-footer-like">1</span>

                            </td>
                            <td>
                                <div class="line-separator-footer-like"></div>
                            </td>
                            <td>
                                <div class="main-footer-like-title" style="vertical-align:middle; margin-top:5px;">DEPOSIT TIME</div>
                                <div class="main-footer-like-description">Lorem ipsum dolor sit<br>amet, consectetur<br>adipiscing elit.</div>
                            </td>
                            <td class="text-footer-like-container">
                                <span class="main-footer-like">2</span>
                            </td>
                            <td>
                                <div class="line-separator-footer-like"></div>
                            </td>
                            <td>
                                <div class="main-footer-like-title" style="vertical-align:middle; margin-top:5px;">CASHOUT TIME</div>
                                <div class="main-footer-like-description">Lorem ipsum dolor sit<br>amet, consectetur<br>adipiscing elit.</div>
                            </td>
                            <td class="text-footer-like-container">
                                <span class="main-footer-like">3</span>
                            </td>
                            <td>
                                <div class="line-separator-footer-like"></div>
                            </td>
                            <td>
                                <div class="main-footer-like-title" style="vertical-align:middle; margin-top:5px;">GET A BONUS</div>
                                <div class="main-footer-like-description">Lorem ipsum dolor sit<br>amet, consectetur<br>adipiscing elit.</div>
                            </td>
                        </tr>
                    </th>
                </table>
            </div>
        </div>
    </div>
    <div class="container" id="main-why-choose-us">
        <div class="row text-center">
            <div class="col-sm-12 content-titles"><strong>WHY CHOOSE US</strong></div>
        </div>
        <hr style="max-width: unset;">
        <div class="row text-center">
            <div class="col-sm-4 content-why-choose-icons">
                <img src="{{asset("assets/icons/1.png")}}"><br>
                <div class="content-why-choose-text">
                    EXTRA 15% CREDITS
                </div>

            </div>
            <div class="col-sm-4 content-why-choose-icons">
                <img src="{{asset("assets/icons/2.png")}}"><br>
                <div class="content-why-choose-text">
                    WEEKLY 5% LOSS REBATE
                </div>

            </div>
            <div class="col-sm-4 content-why-choose-icons">
                <img src="{{asset("assets/icons/3.png")}}"><br>
                <div class="content-why-choose-text">
                    RELIABLE IN WINNING PAYMENT
                </div>

            </div>

            <div class="col-sm-4 content-why-choose-icons">
                <img src="{{asset("assets/icons/1.png")}}"><br>
                <div class="content-why-choose-text">
                    DAILY FAST SETTLEMENT
                </div>

            </div>
            <div class="col-sm-4 content-why-choose-icons">
                <img src="{{asset("assets/icons/2.png")}}"><br>
                <div class="content-why-choose-text">
                    DEPOSITS AND WITHDRAWALS IN SINGAPORE
                </div>

            </div>
            <div class="col-sm-4 content-why-choose-icons">
                <img src="{{asset("assets/icons/3.png")}}"><br>
                <div class="content-why-choose-text">
                    WEEKLY TOP 3 TURNOVER CASH PRIZES GIVEN AWAY
                </div>

            </div>

            <div class="col-sm-4 content-why-choose-icons">
                <img src="{{asset("assets/icons/1.png")}}"><br>
                <div class="content-why-choose-text">
                    GOOD CUSTOMER SUPPORT
                </div>

            </div>
            <div class="col-sm-4 content-why-choose-icons">
                <img src="{{asset("assets/icons/2.png")}}"><br>
                <div class="content-why-choose-text">
                    CLIENTS INFORMATION WILL BE KEPT IN THE STRICTEST CONFIDENCE
                </div>

            </div>

        </div>
    </div>
</div>
@include('layouts.footer')
@include('layouts.footer-script')
</body>

</html>
