<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});
Route::get('/promotions', function () {
    return view('promotions');
});
Route::get('/termsandconditions', function () {
    return view('terms');
});

Route::get('/contactus', function () {
    return view('contact-us');
});

Route::get('/aboutus', function () {
    return view('about-us');
});
Route::get('/dashboard', function () {
    return view('dashboard');
});